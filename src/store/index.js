import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        list: [],
        // 文本框中的内容
        inputValue: 'aaa',
        nextId: 5,
        viewKey: 'all'
    },
    mutations: {
        initList(state, list) {
            state.list = list
        },
        setInputValue(state, val) {
            state.inputValue = val
        },
        // 添加任务事项
        addItem(state) {
            const obj = {
                id: state.nextId,
                info: state.inputValue.trim(),
                done: false
            }
            state.list.push(obj)
            state.nextId++
                state.inputValue = ''
        },
        // 删除任务事项
        removeItem(state, id) {
            // 通过id找到对应的索引，通过索引找到对应的元素
            const i = state.list.findIndex(x => x.id === id)
            if (i !== -1) {
                state.list.splice(i, 1)
            }
        },
        // 修改列表项中的选中状态
        changeStatus(state, param) {
            const i = state.list.findIndex(x => x.id === param.id)
            if (i !== -1) {
                state.list[i].done = param.status
            }
        },
        // 清除已完成的任务事项
        cleanDone(state) {
            state.list = state.list.filter(x => x.done === false)
        },
        // 修改视图的关键字
        changeViewKey(state, key) {
            state.viewKey = key
        }
    },
    actions: {
        getList(context) {
            axios.get('/list.json').then(({ data }) => {
                // console.log(data)
                context.commit('initList', data)
            })
        }
    },
    getters: {
        // 统计未完成的任务条数
        unDoneLength(state) {
            return state.list.filter(x => x.done === false).length
        },
        // 点击按钮时，显示对应的数据列表
        infoList(state) {
            if (state.viewKey === 'all') {
                return state.list
            }
            if (state.viewKey === 'undone') {
                return state.list.filter(x => !x.done)
            }
            if (state.viewKey === 'done') {
                return state.list.filter(x => x.done)
            }
            return state.list
        }
    },
    modules: {}
})